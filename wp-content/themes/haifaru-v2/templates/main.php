<?php
/*
Template Name: Главная страница
*/

get_header();
$pageFields = get_fields();

$breakingPosts = '';
if(!$pageFields['run_last_posts']){
    $breakingPosts = $pageFields['run_posts'];
}

if(!$pageFields['is_breaking']){
    $breaking = get_posts([
        'posts_per_page'   => ($pageFields['run_last_posts']) ? 3 : -1,
        'include'          => $breakingPosts,
        'orderby'          => 'date',
        'order'            => 'DESC',
    ]);
}

$firstMainBlock = FALSE;
$secondMainBlock = FALSE;
$thirdMainBlock = FALSE;

if($pageFields['main_block_auto']){
    //@TODO
}else{
    $firstMainBlock = $pageFields['first_main_block'];
    $secondMainBlock = $pageFields['second_main_block'];
    $thirdMainBlock = $pageFields['third_main_block'];
}


?>
<?php if(!$pageFields['is_breaking']): ?>
<section class="bg-accent add-top-margin">
    <div class="container">
        <div class="row no-gutters d-flex align-items-center">
            <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                <div class="topic-box mt-10 mb-10"><?php _e('Top news', TEXTDOMAIN); ?></div>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-8 col-6">
                <div class="feeding-text-dark">
                    <ol id="sample" class="ticker">
                        <?php foreach ($breaking as $bNews): ?>
                            <li>
                                <a href="<?= get_the_permalink($bNews) ?>"><?= $bNews->post_title ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<!--Main news block start-->
<section class="bg-accent section-space-bottom-less4">
    <div class="container">
        <div class="row tab-space2">
            <?php if($firstMainBlock): ?>
            <div class="col-md-8 col-sm-12 mb-4">
                <div class="img-overlay-70 h-100 img-scale-animate">
                    <img src="<?= postThumb($firstMainBlock); ?>" alt="news" class="img-fluid width-100 h-100">
                    <div class="mask-content-lg">
                        <div class="topic-box-sm color-cinnabar mb-20"><?= get_the_category($firstMainBlock->ID)[0]->name; ?></div>
                        <div class="post-date-light">
                            <ul>
                                <li>
                                    <span><?php _e('Author', TEXTDOMAIN); ?></span>
                                    <a href="<?= get_author_posts_url($firstMainBlock->post_author) ?>"><?= get_userdata($firstMainBlock->post_author)->display_name; ?></a>
                                </li>
                                <li>
                                                <span>
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                </span><?= get_the_date('d/m/Y', $firstMainBlock->ID); ?></li>
                            </ul>
                        </div>
                        <h1 class="title-medium-light d-none d-sm-block">
                            <a href="<?= get_permalink($firstMainBlock) ?>"><?= $firstMainBlock->post_title ?></a>
                        </h1>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="col-md-4 col-sm-12">
                <?php if($secondMainBlock): ?>
                <div class="img-overlay-70 img-scale-animate mb-4">
                    <div class="mask-content-sm">
                        <div class="topic-box-sm color-razzmatazz mb-10"><?= get_the_category($secondMainBlock->ID)[0]->name; ?></div>
                        <h3 class="title-medium-light">
                            <a href="<?= get_permalink($secondMainBlock) ?>"><?= $secondMainBlock->post_title ?></a>
                        </h3>
                    </div>
                    <img src="<?= postThumb($secondMainBlock); ?>" alt="<?= get_the_category($secondMainBlock->ID)[0]->name; ?>" title="<?= $secondMainBlock->post_title ?>" class="img-fluid width-100">
                </div>
                <?php endif; ?>
                <?php if($thirdMainBlock): ?>
                <div class="img-overlay-70 img-scale-animate mb-4">
                    <div class="mask-content-sm">
                        <div class="topic-box-sm color-apple mb-10"><?= get_the_category($thirdMainBlock->ID)[0]->name; ?></div>
                        <h3 class="title-medium-light">
                            <a href="<?= get_permalink($thirdMainBlock) ?>"><?= $thirdMainBlock->post_title ?></a>
                        </h3>
                    </div>
                    <img src="<?= postThumb($thirdMainBlock); ?>" title="<?= $thirdMainBlock->post_title ?>" alt="<?= get_the_category($thirdMainBlock->ID)[0]->name; ?>" class="img-fluid width-100">
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!--Main news block end-->

<?php if(!$pageFields['main_category_off'] && $pageFields['main_category_to_show']): ?>
<!--Top stories start-->
<section class="bg-accent section-space-bottom">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="ne-isotope">
                    <div class="topic-border color-cinnabar mb-30">
                        <div class="topic-box-lg color-cinnabar"><?php _e('Top stories', TEXTDOMAIN); ?></div>
                        <div class="isotope-classes-tab isotop-btn">
                            <?php foreach ($pageFields['main_category_to_show'] as $mainCat): $currentCat = 'current'; ?>
                                <?php if(getTermPosts($mainCat, 'post')): ?>
                                <a href="#" data-filter=".<?= $mainCat->slug ?>" class="<?= $currentCat ?>"><?= $mainCat->name; ?></a>
                                <?php endif; ?>
                            <?php
                            $currentCat = '';
                            endforeach;
                            ?>
                        </div>
                        <div class="more-info-link">
                            <a href="#"><?php _e('All categories', TEXTDOMAIN); ?>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="featuredContainer">
                        <?php foreach ($pageFields['main_category_to_show'] as $mainCat): $catPosts = getTermPosts($mainCat, 'post', 7); ?>
                            <?php if($catPosts): ?>
                                <div class="row <?= $mainCat->slug ?>">
                                    <div class="col-xl-4 col-lg-5 col-md-12 mb-30">
                                        <div class="img-overlay-70 h-100 img-scale-animate">
                                            <a href="<?= get_the_permalink($catPosts[0]) ?>">
                                                <img src="<?= postThumb($catPosts[0]); ?>" alt="<?= $catPosts[0]->post_title ?>" class="img-fluid width-100">
                                            </a>
                                            <div class="mask-content-lg">
                                                <div class="topic-box-sm color-apple mb-20"><?= $mainCat->name ?></div>
                                                <div class="post-date-light">
                                                    <ul>
                                                        <li>
                                                            <span><?php _e('Author', TEXTDOMAIN); ?></span>
                                                            <a href="<?= get_author_posts_url($catPosts[0]->post_author) ?>"><?= get_userdata($catPosts[0]->post_author)->display_name; ?></a>
                                                        </li>
                                                        <li>
                                                                <span>
                                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                </span><?= get_the_date('d/m/Y', $catPosts[0]->ID); ?></li>
                                                    </ul>
                                                </div>
                                                <h2 class="title-medium-light size-lg">
                                                    <a href="<?= get_the_permalink($catPosts[0]) ?>"><?= $catPosts[0] -> post_title; unset($catPosts[0]);?></a>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-8 col-lg-7 col-md-12">
                                        <div class="row">
                                            <?php foreach ($catPosts as $smallPost): ?>
                                            <div class="col-sm-6 col-12">
                                                <div class="media bg-body item-shadow-gray mb-30 d-flex">
                                                    <a class="d-block thumb-link pl-0 col-4" href="<?= get_the_permalink($smallPost) ?>">
                                                        <div class="h-100 post-thumb-wrapper" style="background-image: url(<?= postThumb($smallPost); ?>)" title="<?= $smallPost->post_title; ?>"></div>
                                                    </a>
                                                    <div class="media-body media-padding10 col-8">
                                                        <div class="post-date-dark">
                                                            <ul>
                                                                <li>
                                                                        <span>
                                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                        </span><?= get_the_date('d/m/Y', $smallPost->ID); ?></li>
                                                            </ul>
                                                        </div>
                                                        <h3 class="title-medium-dark mb-none">
                                                            <a href="<?= get_the_permalink($smallPost) ?>"><?= $smallPost->post_title; ?></a>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
        <?php
        $advBlock_1_link = get_field('adv_block_1', 'options');
        $advBlock_1_banner = get_field('adv_block_banner_1', 'options');
        if($advBlock_1_banner):
        ?>
        <div class="row">
            <div class="col-12">
                <div class="ne-banner-layout1 mt-20-r text-center">
                    <?php if($advBlock_1_link): ?><a href="<?= $advBlock_1_link ?>"><?php endif; ?>
                        <img src="<?= $advBlock_1_banner['url'] ?>" alt="ad" class="img-fluid">
                        <?php if($advBlock_1_link): ?></a><?php endif; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>
<!--Top stories end-->
<?php endif; ?>

<!--Focus Category start-->
<section class="bg-accent section-space-bottom-less30">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="topic-border color-persian-green mb-30">
                    <div class="topic-box-lg color-persian-green">International</div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 mb-30">
                        <div class="img-overlay-70 img-scale-animate">
                            <img src="img/news/news69.jpg" alt="news" class="img-fluid width-100">
                        </div>
                        <ul class="item-box-light-mix item-shadow-gray">
                            <li>
                                <div class="post-date-dark">
                                    <ul>
                                        <li>
                                                        <span>
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                        </span>February 10, 2017</li>
                                    </ul>
                                </div>
                                <h3 class="title-medium-dark">
                                    <a href="single-news-3.html">Erik Jones has day he won’t soon forget Denny backup at Bristol</a>
                                </h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="media bg-body item-shadow-gray mb-30">
                            <a class="img-opacity-hover width34-lg width30-md" href="single-news-1.html">
                                <img src="img/news/news70.jpg" alt="news" class="img-fluid">
                            </a>
                            <div class="media-body media-padding15">
                                <div class="post-date-dark">
                                    <ul>
                                        <li>
                                                        <span>
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                        </span>February 10, 2017</li>
                                    </ul>
                                </div>
                                <h3 class="title-medium-dark mb-none">
                                    <a href="single-news-2.html">Can Be Monit roade year with Program.</a>
                                </h3>
                            </div>
                        </div>
                        <div class="media bg-body item-shadow-gray mb-30">
                            <a class="img-opacity-hover width34-lg width30-md" href="single-news-2.html">
                                <img src="img/news/news71.jpg" alt="news" class="img-fluid">
                            </a>
                            <div class="media-body media-padding15">
                                <div class="post-date-dark">
                                    <ul>
                                        <li>
                                                        <span>
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                        </span>June 06, 2017</li>
                                    </ul>
                                </div>
                                <h3 class="title-medium-dark mb-none">
                                    <a href="single-news-3.html">Can Be Monit roade year with Program.</a>
                                </h3>
                            </div>
                        </div>
                        <div class="media bg-body item-shadow-gray mb-30">
                            <a class="img-opacity-hover width34-lg width30-md" href="single-news-3.html">
                                <img src="img/news/news72.jpg" alt="news" class="img-fluid">
                            </a>
                            <div class="media-body media-padding15">
                                <div class="post-date-dark">
                                    <ul>
                                        <li>
                                                        <span>
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                        </span>August 22, 2017</li>
                                    </ul>
                                </div>
                                <h3 class="title-medium-dark mb-none">
                                    <a href="single-news-1.html">Can Be Monit roade year with Program.</a>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ne-sidebar sidebar-break-md col-lg-4 col-md-12">
                <div class="sidebar-box">
                    <div class="topic-border color-cod-gray mb-30">
                        <div class="topic-box-lg color-cod-gray">Stay Connected</div>
                    </div>
                    <ul class="stay-connected-light overflow-hidden">
                        <li class="facebook">
                            <a href="#">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                <div class="connection-quantity">50.2 k</div>
                                <p>Fans</p>
                            </a>
                        </li>
                        <li class="twitter">
                            <a href="#">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                <div class="connection-quantity">10.3 k</div>
                                <p>Followers</p>
                            </a>
                        </li>
                        <li class="linkedin">
                            <a href="#">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                                <div class="connection-quantity">25.4 k</div>
                                <p>Fans</p>
                            </a>
                        </li>
                        <li class="rss">
                            <a href="#">
                                <i class="fa fa-rss" aria-hidden="true"></i>
                                <div class="connection-quantity">20.8 k</div>
                                <p>Subscriber</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="sidebar-box">
                    <div class="ne-banner-layout1 text-center">
                        <a href="#">
                            <img src="img/banner/banner11.jpg" alt="ad" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Focus Category end-->



<?php get_footer(); ?>

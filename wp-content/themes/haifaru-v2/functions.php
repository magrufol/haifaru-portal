<?php

defined('ABSPATH') or die("No script kiddies please!");
define('THEMEPATH_URI', get_template_directory_uri());
define('THEMEDIR', get_template_directory());
define('ASSETS', get_template_directory_uri() . '/assets/');
define('IMAGES', get_template_directory_uri() . '/assets/img/');
define('CSS', get_template_directory_uri() . '/assets/css/');
define('JS', get_template_directory_uri() . '/assets/js/');
define('VENDOR', get_template_directory_uri() . '/assets/vendor/');
define('TEXTDOMAIN', 'haifa');

load_theme_textdomain( TEXTDOMAIN, THEMEDIR .'/languages' );
add_filter('show_admin_bar', '__return_false');
register_nav_menus();
add_theme_support( 'post-thumbnails' );

function disable_wp_emojicons( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}
add_action( 'init', 'disable_wp_emojicons' );




function scriptsInit(){

    //========================CSS========================
    wp_enqueue_style('normalize', CSS . 'normalize.css', false, null);
    wp_enqueue_style('main', CSS . 'main.css', false, null);
    wp_enqueue_style('bootstrap', CSS . 'bootstrap.min.css', false, null);
    wp_enqueue_style('animate', CSS . 'animate.min.css', false, null);
    wp_enqueue_style('font-awesome', CSS . 'font-awesome.min.css', false, null);
    wp_enqueue_style('owl', VENDOR . 'OwlCarousel/owl.carousel.min.css', false, null);
    wp_enqueue_style('owl-theme', VENDOR . 'OwlCarousel/owl.theme.default.min.css', false, null);
    wp_enqueue_style('menu', CSS . 'meanmenu.min.css', false, null);
    wp_enqueue_style('popup', CSS . 'magnific-popup.css', false, null);
    wp_enqueue_style('hover', CSS . 'hover-min.css', false, null);
    wp_enqueue_style('app', ASSETS . 'app.css', false, null);
    wp_enqueue_style('primary-style', get_stylesheet_uri());


    if(is_404()){
        //wp_enqueue_style('404', $cssDir . '404.css', false, null);
    }




    //========================JS========================
    wp_enqueue_script('modernizr', JS .'modernizr-2.8.3.min.js', array('jquery'), null, false);
    if( !is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('jquery', JS . 'jquery-2.2.4.min.js', false);
        wp_enqueue_script('jquery');
    }
    wp_enqueue_script('plugins', JS .'plugins.js', array('jquery'), null, true);
    wp_enqueue_script('popper', JS .'popper.js', array('jquery'), null, true);
    wp_enqueue_script('bootstrap', JS .'bootstrap.min.js', array('jquery'), null, true);
    wp_enqueue_script('wow', JS .'wow.min.js', array('jquery'), null, true);
    wp_enqueue_script('owl', VENDOR .'OwlCarousel/owl.carousel.min.js', array('jquery'), null, true);
    wp_enqueue_script('menu', JS .'jquery.meanmenu.min.js', array('jquery'), null, true);
    wp_enqueue_script('scroll', JS .'jquery.scrollUp.min.js', array('jquery'), null, true);
    wp_enqueue_script('counter', JS .'jquery.counterup.min.js', array('jquery'), null, true);
    wp_enqueue_script('waypoints', JS .'waypoints.min.js', array('jquery'), null, true);
    wp_enqueue_script('isotope', JS .'isotope.pkgd.min.js', array('jquery'), null, true);
    wp_enqueue_script('popup', JS .'jquery.magnific-popup.min.js', array('jquery'), null, true);
    wp_enqueue_script('ticker', JS .'ticker.js', array('jquery'), null, true);
    wp_enqueue_script('main', JS .'main.js', array('jquery'), null, true);

}

add_action('wp_enqueue_scripts', 'scriptsInit');


function registerMenus() {
    register_nav_menus(
        array(
            'main-menu' => __( 'Main Menu' ),
            'side-menu' => __( 'Side Menu' ),
        )
    );
}
add_action( 'init', 'registerMenus' );

add_filter('nav_menu_css_class' , 'addActiveClass' , 10 , 2);

function addActiveClass ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

function change_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/','/ class="ne-dropdown-menu" /',$menu);
    return $menu;
}
add_filter('wp_nav_menu','change_submenu_class');

function postThumb($post){
    $postThumb = IMAGES . 'img-placeholder.jpg';
    if($post && get_the_post_thumbnail_url($post)){
        $postThumb = get_the_post_thumbnail_url($post);
    }

    return $postThumb;
}

function getTermPosts($term, $post_type, $post_count = -1){

    $posts = get_posts([
        'posts_per_page' => $post_count,
        'post_type' => $post_type,
        'post_status' => 'publish',
        'tax_query' => array(
            array(
                'taxonomy' => $term->taxonomy,
                'field' => 'id',
                'terms' => $term->term_id, // Where term_id of Term 1 is "1".
                'include_children' => true
            )
        )
    ]);

    if($posts){
        return $posts;
    }

    return FALSE;
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Рекламные блоки',
        'menu_title'	=> 'Рекламные блоки',
        'menu_slug' 	=> 'adv_blocks',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));


}

add_filter('acf/settings/save_json', 'acf_json_save');

function acf_json_save( $path ) {

    $path = get_stylesheet_directory() . '/acf-json';
    return $path;

}

add_filter('acf/settings/load_json', 'acf_json_load');

function acf_json_load( $paths ) {

    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/my-custom-folder';
    return $paths;

}

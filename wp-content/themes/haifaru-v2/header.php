<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo get_bloginfo(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?php IMAGES ?>/img/favicon.png">
    <?php wp_head(); ?>
    <!-- For IE -->
    <link rel="stylesheet" type="text/css" href="<?php echo CSS ?>/ie-only.css" />
</head>

<body <?php body_class(); ?>>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an
    <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<![endif]-->
<div id="preloader"></div>

<div id="wrapper" class="wrapper">
    <!-- Header Area Start Here -->
    <header>
        <div id="header-layout2" class="header-style2">
            <div class="header-top-bar">
                <div class="top-bar-top bg-accent border-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-md-12">
                                <ul class="news-info-list text-center--md">
                                    <li>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>Australia</li>
                                    <li>
                                        <i class="fa fa-calendar" aria-hidden="true"></i><span id="current_date"></span></li>
                                    <li>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>Last Update 11.30 am</li>
                                    <li>
                                        <i class="fa fa-cloud" aria-hidden="true"></i>29&#8451; Sydney, Australia</li>
                                </ul>
                            </div>
                            <div class="col-lg-4 d-none d-lg-block">
                                <ul class="header-social">
                                    <li>
                                        <a href="#" title="facebook">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="twitter">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="google-plus">
                                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/feed/rss/" title="rss">
                                            <i class="fa fa-rss" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-bar-bottom bg-body pt-20 d-none d-lg-block">
                    <div class="container">
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="logo-area">
                                    <a href="/" class="img-fluid">
                                        <img src="<?php echo IMAGES ?>logo_full.png" alt="HaifaRu">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <div class="ne-banner-layout1 pull-right">
                                    <a href="#">
                                        <img src="img/banner/banner2.jpg" alt="ad" class="img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-menu-area bg-body" id="sticker">
                <div class="container">
                    <div class="row no-gutters d-flex align-items-center">
                        <div class="col-lg-10 position-static d-none d-lg-block">
                            <div class="ne-main-menu">

                                <?php

                                wp_nav_menu( array(
                                    'theme_location'  => 'main-menu',
                                    'container'       => 'nav',
                                    'container_class' => '',
                                    'container_id'    => 'dropdown',
                                    'menu_class'      => 'main-menu',
                                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    'depth'           => 2,
                                ) );

                                ?>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-12 text-right position-static">
                            <div class="header-action-item on-mobile-fixed">
                                <ul>
                                    <li>
                                        <form id="top-search-form" class="header-search-light">
                                            <input type="text" class="search-input" placeholder="Search...." required="" style="display: none;">
                                            <button class="search-button">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </li>
                                    <li class="d-none d-sm-block d-md-block d-lg-none">
                                        <button type="button" class="login-btn" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-user" aria-hidden="true"></i>Sign in
                                        </button>
                                    </li>
                                    <li>
                                        <div id="side-menu-trigger" class="offcanvas-menu-btn offcanvas-btn-repoint">
                                            <a href="#" class="menu-bar">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </a>
                                            <a href="#" class="menu-times close">
                                                <span></span>
                                                <span></span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

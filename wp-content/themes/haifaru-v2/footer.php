<?php if(has_nav_menu('side-menu')): ?>
<div id="offcanvas-body-wrapper" class="offcanvas-body-wrapper">
    <div id="offcanvas-nav-close" class="offcanvas-nav-close offcanvas-menu-btn">
        <a href="#" class="menu-times re-point">
            <span></span>
            <span></span>
        </a>
    </div>

    <?php
    wp_nav_menu( array(
        'theme_location'  => 'side-menu',
        'container'       => 'div',
        'container_class' => 'offcanvas-main-body',
        'container_id'    => '',
        'menu_class'      => 'side-menu',
        'items_wrap'      => '<ul id="%1$s accordion" class="%2$s offcanvas-nav panel-group">%3$s</ul>',
        'depth'           => 1,
    ) );

    ?>


</div>
<?php endif; ?>
</div>
<!--wrapper end-->
<?php wp_footer(); ?>
</body>
</html>

<?php

//Rekl
$rekl1 = get_field('home_right_top', 'options');


//get tags
$tags = get_tags(array('orderby' => 'count'));

?>


<div class="sidebar"><!-- sidebar -->

    <div class="widget search"><!-- Search Widget -->
        <h4>Поиск по сайту</h4>
        <div class="widget-wrap">
            <?php get_search_form(); ?>
        </div>
    </div>

    <div class="widget newsletter"><!-- Newsletter Subscription -->
        <h4>Погода</h4>
        <div class="widget-wrap">
            <!-- Gismeteo informer START -->
            <link rel="stylesheet" type="text/css" href="https://nst1.gismeteo.ru/assets/flat-ui/legacy/css/informer.min.css">
            <div id="gsInformerID-Ib35H7DitR8R4o" class="gsInformer" style="width:295px;height:227px">
                <div class="gsIContent">
                    <div id="cityLink">
                        <a href="https://www.gismeteo.ru/weather-haifa-5396/" target="_blank">Погода в Хайфе</a>
                    </div>
                    <div class="gsLinks">
                        <table>
                            <tr>
                                <td>
                                    <div class="leftCol">
                                        <a href="https://www.gismeteo.ru/" target="_blank">
                                            <img alt="Gismeteo" title="Gismeteo" src="https://nst1.gismeteo.ru/assets/flat-ui/img/logo-mini2.png" align="middle" border="0" />
                                            <span>Gismeteo</span>
                                        </a>
                                    </div>
                                    <div class="rightCol">
                                        <a href="https://www.gismeteo.ru/weather-haifa-5396/2-weeks/" target="_blank">Прогноз на 2 недели</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <script async src="https://www.gismeteo.ru/api/informer/getinformer/?hash=Ib35H7DitR8R4o" type="text/javascript"></script>
            <!-- Gismeteo informer END -->
        </div>
    </div>

    <div class="widget newsletter"><!-- Newsletter Subscription -->
        <h4>Рассылка новостей</h4>
        <div class="widget-wrap">
            <p>Хотите получить полезную информацию об интересных мероприятиях, проводимых в Хайфе? Наша многолетняя интернет-рассылка вам о них расскажет</p>
            <?php echo do_shortcode('[contact-form-7 id="5" title="Newsletter" html_class="use-floating-validation-tip"]'); ?>
        </div>
    </div>


    <div class="widget rekl1"><!-- Author Widget -->
        <h4>Реклама</h4>
        <div class="widget-wrap">
            <?php if(have_rows('home_right_top', 'options')): ?>
                <?php while(have_rows('home_right_top', 'options')): the_row(); ?>
                    <?php if(get_sub_field('rekl1_url')): ?>
                        <a href="<?php the_sub_field('rekl1_url'); ?>" rel="nofollow" target="_blank">
                            <img src="<?php the_sub_field('rekl1_image'); ?>" class="img-responsive">
                        </a>
                    <?php else: ?>
                        <img src="<?php the_sub_field('rekl1_image'); ?>" class="img-responsive">
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>


        <div class="widget-wrap">
            <?php if(have_rows('home_right_bottom', 'options')): ?>
                <?php while(have_rows('home_right_bottom', 'options')): the_row(); ?>
                    <?php if(get_sub_field('link')): ?>
                        <a href="<?php the_sub_field('link'); ?>" rel="nofollow" target="_blank">
                            <img src="<?php the_sub_field('image'); ?>" class="img-responsive">
                        </a>
                    <?php else: ?>
                        <img src="<?php the_sub_field('image'); ?>" class="img-responsive">
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="widget social"><!-- Social Media Connection -->
        <h4>Мы в соц. сетях</h4>
        <div class="widget-wrap">
            <ul class="list-inline social"><!-- Social Share -->
                <li><a href="https://www.facebook.com/groups/228377432160/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            </ul>
        </div>
    </div>

    <div class="widget">
        <?php if ( is_active_sidebar( 'calendar' ) ) : ?>
            <h4>Календарь новостей</h4>
            <?php dynamic_sidebar( 'calendar' ); ?>
        <?php endif; ?>
    </div>


    <?php if($tags): ?>
        <div class="widget tags"><!-- Tags -->
            <h4>Метки</h4>
            <div class="widget-wrap">
                <ul class="list-inline tags"><!-- Social Share -->
                    <?php foreach ($tags as $tag): ?>
                        <li><a href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

    <?php endif; ?>



</div><!-- end sidebar -->

<?php
defined('ABSPATH') or die("No script kiddies please!");


////remove_action('wp_head', 'rsd_link');
//remove_action('wp_head', 'wp_generator');
////remove_action('wp_head', 'feed_links', 2);
//remove_action('wp_head', 'index_rel_link');
////remove_action('wp_head', 'wlwmanifest_link');
////remove_action('wp_head', 'feed_links_extra', 3);
//remove_action('wp_head', 'start_post_rel_link', 10, 0);
//remove_action('wp_head', 'parent_post_rel_link', 10, 0);
//remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
add_filter('show_admin_bar', '__return_false');
register_nav_menus();
add_theme_support( 'post-thumbnails' );

function disable_wp_emojicons() {
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

//stop cf7 scripts load
//add_filter( 'wpcf7_load_js', '__return_false' );
//add_filter( 'wpcf7_load_css', '__return_false' );

//disable REST API
//remove_action( 'wp_head','rest_output_link_wp_head');
//remove_action( 'wp_head','wp_oembed_add_discovery_links');
//remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}
add_action( 'init', 'disable_wp_emojicons' );

function scriptsInit(){
    $tmpDir = get_template_directory_uri();
    $cssDir = $tmpDir . '/assets/css/';
    $jsDir = $tmpDir . '/assets/js/';


    //========================CSS========================
    wp_enqueue_style('bootstrap', $cssDir . 'bootstrap.css', false, null);
    wp_enqueue_style('font-awesome', $tmpDir . '/assets/fonts/font-awesome/css/font-awesome.css', false, null);
    wp_enqueue_style('owl-carousel', $cssDir . 'owl.carousel.css', false, null);
    wp_enqueue_style('owl-theme', $cssDir . 'owl.theme.css', false, null);
    wp_enqueue_style('primary-style', get_stylesheet_uri());
    wp_enqueue_style('responsive', $cssDir . 'responsive.css', false, null);

    if(is_404()){
        wp_enqueue_style('404', $cssDir . '404.css', false, null);
    }



    //========================JS========================
    wp_enqueue_script('modernizr', $jsDir.'modernizr.custom.js', array('jquery'), null, false);
    wp_enqueue_script('bootstrap', $jsDir.'bootstrap.js', array('jquery'), null, true);
    wp_enqueue_script('owl-carousel', $jsDir.'owl.carousel.js', array('jquery'), null, true);
    wp_enqueue_script('smoothscroll', $jsDir.'SmoothScroll.js', array('jquery'), null, true);
    wp_enqueue_script('main', $jsDir.'main.js', array('jquery'), null, true);

}

add_action('wp_enqueue_scripts', 'scriptsInit');


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Другие настройки',
        'menu_title'	=> 'Другие настройки',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
}

function custom_pagination($numpages = '', $pagerange = '', $paged='') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     *
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if(!$numpages) {
            $numpages = 1;
        }
    }

    $pagination_args = array(
        'base'            => '%_%',
        'format'          => '?page=%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => False,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => True,
        'prev_text'       => __('&laquo;'),
        'next_text'       => __('&raquo;'),
        'type'            => 'array',
        'add_args'        => false,
        'add_fragment'    => '',
    );

    $paginate_links = paginate_links($pagination_args);

    $output = '<nav id="tf-pagination"><ul class="pagination">';

    if($paginate_links){
        foreach ($paginate_links as $pag){
            $node = '<li>';
            $node .= $pag;
            $node .= '</li>';

            $output .= $node;
        }

        $output .= '</ul></nav>';

        echo $output;
    }
}


function calendarWidget() {

    register_sidebar( array(
        'name'          => 'Календарь новостей',
        'id'            => 'calendar',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );

}
add_action( 'widgets_init', 'calendarWidget' );
<?php

get_header();
the_post();
$postID = get_the_ID();
$newsCategory = get_the_category();
$tags = wp_get_post_tags($postID);
$shareURL = get_the_permalink();

$relatedPostsArgs = array(
    'posts_per_page' => 3,
    'exclude' => $postID,
    'tax_query' => array(
        array(
            'taxonomy' => 'post_tag',
            'field' => 'slug',
            'terms' => $tags[1]->slug
        )
    )
);

$relatedPosts = get_posts($relatedPostsArgs);
$postRekl = get_field('post_rkl', 'options');

$postDateDay = get_the_date('d');
$postDateMonth = get_the_date('F');
$newsCategory = get_the_category();
$newsAuthor = get_the_author();
$shareURL = get_the_permalink();

?>


<!-- Blog Body
====================================-->
<div id="tf-content" class="layout-two">
    <div class="container"><!-- Container -->
        <div class="row"><!-- Row -->

            <div class="col-md-8"> <!-- Left Content for Blog Posts -->
                <div class="blog-post"> <!-- Blog Posts -->

                    <div class="post-detail"><!-- Page Basic Info (title, featured image, metas) -->
                        <a href="" class="meta-date"><span class="bolder"><?php echo $postDateDay ?></span><br><?php echo $postDateMonth ?></a>
                        <h2><?php the_title(); ?></h2> <!-- Post Title-->

                        <div class="img-wrap"> <!-- Image Wrap -->
                            <div class="metas">
                                <a href="<?php echo get_category_link($newsCategory[0]->term_id) ?>" class="meta-cat"><?php echo $newsCategory[0]->name; ?></a>
                            </div>
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive home-news-preview" alt="<?php the_title(); ?>">
                            <p class="image-desc"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></p>
                        </div>
                    </div><!-- end Page Basic Info (featured image, metas) -->

                    <div class="post-content">
                        <!-- Post Main Content -->
                        <?php the_content(); ?>
                    </div>

                    <?php if($postRekl): ?>
                    <div class="post-content" style="border-top: 5px solid #fff;">
                        <?php echo $postRekl; ?>
                    </div>
                    <?php endif; ?>

                    <ul class="list-inline social"><!-- Social Share -->
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $shareURL; ?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/home?status=<?php echo $shareURL; ?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://plus.google.com/share?url=<?php echo $shareURL; ?>"><i class="fa fa-google-plus"></i></a></li>
                    </ul>

                    <?php if($tags): ?>
                        <h4 class="text-center">Метки</h4>
                        <ul class="list-inline tags">
                            <?php foreach ($tags as $tag): ?>
                                <li><a href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                </div><!-- end Blog Post -->


                <?php if($relatedPosts): ?>
                    <div class="related-posts"> <!-- Lists of Related Posts -->
                        <h4 class="title">Похожие новости</h4>
                        <div class="row">
                            <?php foreach ($relatedPosts as $post): setup_postdata($post);  ?>
                                <div class="col-md-4">
                                    <div class="thumb-post"><!-- Thumbnail Post #1 -->
                                        <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url( 'thumbnail' );  ?>" class="img-responsive" alt="Image"></a>
                                        <h3 class="media-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    </div>
                                </div>
                            <?php endforeach; wp_reset_postdata(); ?>
                        </div>
                    </div> <!--  end Lists of Related Posts -->
                <?php endif; ?>



                <div class="comments"> <!-- Lists of Related Posts -->
                    <h4 class="title">Комментарии</h4>
<!--                    <h3>Комментарии которые вы публикуете на сайте будут видны так же на нашей странице в Facebook</h3>-->

                    <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="50"></div>

                </div>


            </div><!-- End of Posts/Blogrol -->


            <div class="col-md-4"> <!-- Right content for sidebar and widget area-->
                <?php get_template_part( 'sidebar' ); ?>
            </div><!-- end Right content for sidebar and widget area-->

        </div><!-- end Row -->
    </div><!-- End Container -->
</div>

<div id="tf-page-header" class="layout-two text-center">
    <div class="container">
        <style type='text/css'>
            #iibravo20seller {width:1100px;font-family:Arial;border:0px #FFFFFF double;padding:0px;background:#FFFFFF}
            #iibravo20seller p {padding:0;margin:0}
            #iibravo20seller a {color:#0000FF;text-decoration:underline}
            #iibravo20seller a:hover {color:#CC0000;text-decoration:underline}
            #iibravo20seller img {margin:3px 10px 0 0;display:;width:100px;height:68px;border:0px #FFFFFF solid}
            #iibravo20seller .feedTitle {font-size:12px;display:undefined}
            #iibravo20seller ul {padding:0;margin:0;}
            #iibravo20seller li {list-style-type:none;clear:none;float:left;width:200px;padding-right:20px}
            #iibravo20seller .headline {font-size:14px;font-weight:normal}
            #iibravo20seller div {font-size:12px;color:#000000;}
            #iibravo20seller .date {padding:0;margin:0;}
            #creditfooter {clear:both;display:undefined;padding-top:8px !important;font-size:undefinedpx;text-align:right}
        </style>
        <script type="text/javascript">var nItems = "5";var showTitle = 1;var showImg = 1;var showTxt = 1;var showDate = 1;var showCities = 1;var showTickets = 1;var fName = 0;var links = 1;var sellerLinks = "bravo-haifa.israelinfo.ru";</script>
        <script src="http://bravo-haifa.israelinfo.ru/informer/iibravo20seller_utf.htm" type="text/javascript" ></script>
    </div>
</div>

<?php get_footer(); ?>

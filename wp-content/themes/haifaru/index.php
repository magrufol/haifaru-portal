<?php

get_header();
$tmpDir = get_template_directory_uri();

//Main slider
$recentPosts = get_field('fixed_posts', 'options');
$activeSlide = 'active';
$postCount = 1;
$fixedPostsPointer = array();

//News feed
$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
$newsLoopArgs = array(
    'post_type' => 'post',
    'posts_per_page' => 6,
    'orderby' => 'date',
    'order' => 'DESC',
    'paged' => $paged
);
$news = new WP_Query($newsLoopArgs);

?>
<div id="tf-content">
    <div class="container">
        <div class="row">
            <!-- News container -->
            <div class="col-md-8">
                <?php if($news->have_posts()): ?>
                    <?php while ($news->have_posts()): $news->the_post(); ?>
                        <!-- Post -->
                        <?php
                        $postDateDay = get_the_date('d');
                        $postDateMonth = get_the_date('F');
                        $newsCategory = get_the_category();
                        $newsAuthor = get_the_author();
                        $shareURL = get_the_permalink();
                        ?>
                        <div class="post-block">
                            <div class="post-detail">
                                <a href="" class="meta-date"><span class="bolder"><?php echo $postDateDay ?></span><br><?php echo $postDateMonth ?></a>
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> <!-- Post Title-->

                                <div class="img-wrap"> <!-- Image Wrap -->
                                    <div class="metas">
                                        <a href="<?php echo get_category_link($newsCategory[0]->term_id) ?>" class="meta-cat"><?php echo $newsCategory[0]->name; ?></a>
                                    </div>
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive home-news-preview" alt="<?php the_title(); ?>">
                                </div>

                                <?php the_excerpt(); ?>
                            </div>

                            <a href="<?php the_permalink(); ?>" class="btn btn-default tf-btn txt-link">Читать дальше</a>

                            <ul class="list-inline social"><!-- Social Share -->
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $shareURL; ?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/home?status=<?php echo $shareURL; ?>"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://plus.google.com/share?url=<?php echo $shareURL; ?>"><i class="fa fa-google-plus"></i></a></li>
                            </ul>


                        </div>
                        <!-- end Post -->
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>


                <!-- Pagination -->
                <div class="text-center">
                <?php custom_pagination($news->max_num_pages,"",$paged); ?>
                </div>
                <!-- Pagination end -->

            </div>
            <!-- End of news container -->


            <div class="col-md-4"> <!-- Right content for sidebar and widget area-->
                <?php get_template_part( 'sidebar' ); ?>
            </div><!-- end Right content for sidebar and widget area-->

        </div><!-- end Row -->
    </div><!-- End Container -->
</div>


<?php get_footer(); ?>

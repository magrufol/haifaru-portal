// Hello.
//
// This is The Scripts used for Antelope Minimal Blog Theme
//
//

(function($) {

    function main() {

        (function () {
            'use strict';

            // Smooth Scroll To Top
            //==========================================
            $(function() {
                $('.scroll').on("click",function(){
                    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top - 100
                            }, 1000);
                            return false;
                        }
                    }
                });
            });

            var styles = [
                'color: white'
                , 'display: block'
                , 'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)'
                , 'line-height: 40px'
                , 'text-align: center'
                , 'font-size: 25px'
            ].join(';');

            console.log('%c Случай в театре. Спектакль для детей. Момент, где вот-вот должен появиться главный злодей - свет выключен, оркестр настороженно так жужжит. в зале тишина. И тут такой тоненький детский голосок: "Еб твою мать! Страшно-то как!!!""', styles);


            cheet('i d d q d', function () {
                $( "body" ).append( '<div class="doom"></div>' );
            });

            // Header Slider Disable Auto Slide
            //==========================================
            /* Header Slider Layout #1 */
            $('#header-slider.carousel.layout-one').carousel({
                interval: 3000
            });
            /* Header Slider Layout #2 */
            $('#header-slider.carousel.layout-two').carousel({
                interval: false
            });

            // Slider Toggle
            //==========================================
            $('.thumb-block').on("click",function() {
                $('.thumb-block.active').removeClass('active');
                $(this).addClass('active');
            });

        }());


    }
    main();

})( jQuery );


<form role="search" method="get" action="<?php echo home_url( '/' ); ?>" class="form">
    <div class="input-group">
        <input type="text" name="s" class="form-control" placeholder="Ключевое слово">
        <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Искать</button>
                                    </span>
    </div><!-- /input-group -->
</form>
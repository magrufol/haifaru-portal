<?php
/*
 *  Template name: Contact Page
 */

get_header();
the_post();

?>

<!-- Page Title
    ====================================-->
<div id="tf-page-header" class="layout-two text-center">
    <div class="container">
        <h5><em>Если вам есть что то сказать,</em></h5>
        <hr>
        <h2 class="text-uppercase">Напишите нам</h2>
    </div>
</div>

<!-- Blog Body
    ====================================-->
<div id="tf-content" class="layout-two contact">
    <div class="container"><!-- Container -->
        <div class="row"><!-- Row -->

            <div class="col-md-10 col-md-offset-1"> <!-- About us content -->
                <div class="post-block">
                    <div class="post-detail">
                        <?php echo do_shortcode('[contact-form-7 id="46" title="Main contact form" html_id="contact-form" html_class="form use-floating-validation-tip"]') ?>
                    </div>
                </div>

            </div><!-- endAbout us content -->

        </div><!-- end Row -->
    </div><!-- End Container -->
</div>


<?php get_footer(); ?>


<?php

get_header();

?>


<!-- Page Title  ====================================-->
<div id="tf-page-header" class="layout-two text-center">
    <div class="container">
        <h5><em>Архив новостей</em></h5>
        <h2 class="text-uppercase">
            <?php echo get_the_archive_title( ); ?>
        </h2>
    </div>
</div>

<?php if(have_posts()): ?><div id="tf-content" class="category">
    <div class="container"><!-- Container -->
        <div class="row"><!-- Row -->
            <div class="col-md-10 col-md-offset-1 search-content"> <!-- Left Content for Blog Posts -->
                <?php while(have_posts()): the_post(); ?>
                    <?php
                    $postDateDay = get_the_date('d');
                    $postDateMonth = get_the_date('F');
                    $newsCategory = get_the_category();
                    $newsAuthor = get_the_author();
                    $shareURL = get_the_permalink();
                    ?>

                    <div class="post-block"> <!-- Post -->
                        <div class="post-detail">
                            <a href="#" class="meta-date"><span class="bolder"><?php echo $postDateDay; ?></span><br><?php echo $postDateMonth; ?></a>
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> <!-- Post Title-->

                            <div class="img-wrap"> <!-- Image Wrap -->
                                <div class="metas">
                                    <a href="#" class="meta-cat"><?php echo $newsCategory[0]->name; ?></a>
                                </div>
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive home-news-preview" alt="<?php the_title(); ?>">
                            </div>

                            <?php the_excerpt(); ?>
                        </div>

                        <a href="<?php the_permalink(); ?>" class="btn btn-default tf-btn txt-link">Читать дальше</a>

                        <ul class="list-inline social"><!-- Social Share -->
                            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $shareURL; ?>"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/home?status=<?php echo $shareURL; ?>"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://plus.google.com/share?url=<?php echo $shareURL; ?>"><i class="fa fa-google-plus"></i></a></li>
                        </ul>

                    </div><!-- end Post -->

                <?php endwhile; ?>


                <!-- Pagination -->
                <div class="text-center" style="margin-bottom: 50px;">
                    <?php custom_pagination($query->max_num_pages,"",$paged); ?>
                </div>
                <!-- Pagination end -->


            </div>
        </div>
    </div>
    <?php wp_reset_postdata(); ?>
    <?php else: ?>

        <div class="container"><!-- Container -->
            <div class="row"><!-- Row -->
                <div class="col-md-10 col-md-offset-1 search-content">
                    <h2>К сожалению, мы ничего не нашли. Попробуйте еще раз: </h2>
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>





    <?php

    get_footer();

    ?>

    <?php
    get_footer();
    ?>



<?php

get_footer();

?>
